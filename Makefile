VENV3 := .venv3
VENV2 := .venv2

VENV := ${VENV3}
ACTIVATE := ${VENV}/bin/activate


all: test run

test: ${VENV}
	. ${ACTIVATE} && PYTHONPATH=. pytest --cov=wordpair

test3: ${VENV3} requirements.txt
	. ${VENV3}/bin/activate && PYTHONPATH=. pytest --cov=wordpair

test2: ${VENV2} requirements.txt
	. ${VENV2}/bin/activate && PYTHONPATH=. pytest --cov=wordpair

venv: ${VENV}

${VENV3}: requirements.txt
	test -d ${VENV3} || virtualenv --python=python3 ${VENV3}
	. ${VENV3}/bin/activate; env | grep VIRTUAL
	${VENV3}/bin/pip install 'setuptools>=17.1'
	${VENV3}/bin/pip install -r requirements.txt

${VENV2}: requirements.txt
	test -d ${VENV2} || virtualenv ${VENV2}
	. ${VENV2}/bin/activate; env | grep VIRTUAL
	${VENV2}/bin/pip install 'setuptools>=17.1'
	${VENV2}/bin/pip install -r requirements.txt

run: ${VENV}
	. ${ACTIVATE}
	FLASK_APP=wordpair.py flask run

debug: ${VENV}
	. ${ACTIVATE}
	FLASK_APP=wordpair.py FLASK_DEBUG=1 flask run

clean:
	rm -rf ${VENV2} ${VENV3}
