from __future__ import print_function, unicode_literals

from wordpair.app import app


def main():
    import argparse

    argparser = argparse.ArgumentParser(add_help=False)
    argparser.add_argument('-h', '--host', default='0.0.0.0')
    argparser.add_argument('-p', '--port', default=5000, type=int)
    argparser.add_argument('--help', action='help')
    args = argparser.parse_args()

    app.run(host=args.host, port=args.port)


if __name__ == '__main__':
    main()
