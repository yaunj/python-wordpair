#!/usr/bin/env python
from __future__ import print_function, unicode_literals
from random import choice


class Dictionary:
    def __init__(self, source_file=None, wordclass_mapping=None):
        if source_file:
            self.source_file = source_file
        if wordclass_mapping:
            self.wordclass_mapping = wordclass_mapping

        self.words = self.parse_dictionary()

    def parse_dictionary(self):
        return {}

    def get_pair(self):
        return self.get_words(self.default_pair)

    def get_words(self, classes):
        words = [choice(self.words[klass]) for klass in classes]
        return {'words': words, 'phrase': ' '.join(w['word'] for w in words)}
