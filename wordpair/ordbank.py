#!/usr/bin/env python
from __future__ import print_function, unicode_literals
from collections import defaultdict
from os import path
import string

from wordpair.basedictionary import Dictionary

here = path.abspath(path.dirname(__file__))
ORDBANK_FILE = path.join(here, 'resources/ordbank/fullform_bm.txt')

mapping = {
    "CLB":        "Ukjent",
    "adj":        "Adjektiv",
    "adjektiv":   "Adjektiv",
    "adv":        "Adverb",
    "det":        "Ukjent",
    "fork":       "Forkortelse",
    "forsk":      "Ukjent",
    "henv":       "Ukjent",
    "i":          "Ukjent",
    "inf":        "Infinitivsmerke",
    "inf-merke":  "Infinitivsmerke",
    "interj":     "Interjeksjon",
    "kon":        "Konjunksjon",
    "konj":       "Konjunksjon",
    "kvant":      "Ukjent",
    "perf-part":  "Ukjent",
    "pref":       "Ukjent",
    "prep":       "Preposisjon",
    "prep-subst": "Preposisjon",
    "pres":       "Ukjent",
    "pron":       "Pronomen",
    "sbu":        "Ukjent",
    "subst":      "Substantiv",
    "symb":       "Symbol",
    "verb":       "Verb",
}


class OrdbankDictionary(Dictionary):
    language = 'no'
    default_pair = ['adj', 'subst']
    source_file = ORDBANK_FILE
    wordclass_mapping = mapping

    def parse_dictionary(self):
        words = defaultdict(list)

        with open(self.source_file, 'rb') as infile:
            for line in infile:
                line = line.decode('latin1')

                if line[0] not in string.digits:
                    continue

                parts = line.split('\t')

                _, base, word, meta, _, _ = parts
                klass = meta.split(' ')[0].split('+')[0]

                # FIXME: Do better
                if " " in word:
                    # Skip dictionary lines with multiple words
                    continue
                if klass == "subst" and "ent ub" not in meta:
                    continue
                if klass == "adj" and "ub ent" not in meta:
                    continue

                wordobj = {
                    'word': word,
                    'class': self.wordclass_mapping[klass],
                    'ordbank_class': klass,
                    'meta': meta.split(' '),
                }

                words[klass].append(wordobj)

        dictionary = {k: list(v) for k, v in words.items()}
        return dictionary


if __name__ == '__main__':
    dictionary = OrdbankDictionary()
    random_words = dictionary.get_pair()
    print(random_words)
    print(random_words['phrase'])

    # import timeit
    # print("parse_dictionary",
    #       timeit.repeat('parse_dictionary()', number=1, globals=locals()))
    # print("get_pair",
    #       timeit.repeat('get_pair(words)', globals=locals()))
