from __future__ import print_function, unicode_literals
from functools import wraps

import flask

import wordpair.moby as moby
import wordpair.ordbank as ordbank

app = flask.Flask(__name__)

mobydict = moby.MobyDictionary()
ordbankdict = ordbank.OrdbankDictionary()


def json_or_text(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        result = func(*args, **kwargs)

        if flask.request.headers['Content-Type'] == 'application/json':
            return flask.jsonify(result)

        if 'phrase' in result:
            return result['phrase']

        return repr(result)

    return decorator


@app.route('/v1/en/')
@json_or_text
def v1_english():
    return mobydict.get_pair()


@app.route('/v1/en/<string:wordclasses>')
@json_or_text
def v1_english_multi(wordclasses):
    classes = wordclasses.split(',')
    words = mobydict.get_words(classes)

    return words


@app.route('/v1/en/wordclasses/')
@json_or_text
def v1_english_wordclasses():
    return moby.mapping


@app.route('/v1/no/')
@json_or_text
def v1_norwegian():
    return ordbankdict.get_pair()


@app.route('/v1/no/<string:wordclasses>')
@json_or_text
def v1_norwegian_multi(wordclasses):
    classes = wordclasses.split(',')
    words = ordbankdict.get_words(classes)

    return words


@app.route('/v1/no/wordclasses/')
@json_or_text
def v1_norwegian_wordclasses():
    return ordbank.mapping


@app.route('/')
@json_or_text
def api_index():
    links = [
        {'rel': 'v1', 'href': flask.url_for('v1_index')}
    ]
    return {'links': links}


@app.route('/v1/')
@json_or_text
def v1_index():
    links = [
        {'rel': 'en', 'href': flask.url_for('v1_english')},
        {'rel': 'en', 'href': flask.url_for('v1_english', wordclasses="AAN")},
        {'rel': 'en', 'href': flask.url_for('v1_english_wordclasses')},
        {'rel': 'no', 'href': flask.url_for('v1_norwegian')},
        {'rel': 'no', 'href': flask.url_for('v1_norwegian', wordclasses="adj,subst")},
        {'rel': 'en', 'href': flask.url_for('v1_norwegian_wordclasses')},
    ]
    return {'links': links}
