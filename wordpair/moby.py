#!/usr/bin/env python
from __future__ import print_function, unicode_literals
from collections import defaultdict
from os import path

from wordpair.basedictionary import Dictionary

here = path.abspath(path.dirname(__file__))
MOBY_FILE = path.join(here, 'resources/mpos/mobyposi.i')

mapping = {
    'N': "Noun",
    'p': "Plural",
    'h': "NounPhrase",
    'V': "Verb",
    't': "VerbTransitive",
    'i': "VerbIntransitive",
    'A': "Adjective",
    'v': "Adverb",
    'C': "Conjunction",
    'P': "Preposition",
    '!': "Interjection",
    'r': "Pronoun",
    'D': "ArticleDefinite",
    'I': "ArticleIndefinite",
    'o': "Nominative",
}


class MobyDictionary(Dictionary):
    language = 'en'
    default_pair = 'AN'
    source_file = MOBY_FILE
    wordclass_mapping = mapping

    def parse_dictionary(self):
        words = defaultdict(list)

        with open(self.source_file, 'rb') as infile:
            data = infile.read()

            for pair in data.split(b'\r'):
                parts = pair.split(b'\xd7')

                if len(parts) != 2:
                    continue

                word, klasses = parts
                word = word.decode('macroman')
                klasses = klasses.decode('ASCII')

                wordobj = {
                    'word': word,
                    'moby_classes': klasses,
                }

                for klass in klasses:
                    try:
                        wordobj['class'] = self.wordclass_mapping[klass]
                        words[klass].append(wordobj)
                    except KeyError:
                        print("Problem with word:", word, "classes:", klasses)
                        continue

        return words


if __name__ == '__main__':
    dictionary = MobyDictionary()
    random_words = dictionary.get_pair()
    print(random_words)
    print(random_words['phrase'])

    # import timeit
    # print("parse_dictionary",
    #       timeit.repeat('parse_dictionary()', number=1, globals=locals()))
    # print("get_pair",
    #       timeit.repeat('get_pair(words)', globals=locals()))
