from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
#with open(path.join(here, 'README.md'), encoding='utf-8') as f:
#    long_description = f.read()
long_description = """
wordpair
========

Contains a dictionary with information about word classes. Based on this we
generate random word pairs.

Hit up /en/ for a word pair:

    {
        "phrase": "unavoidable licensor",
        "words": [
            {"word": "unavoidable", "class": "Adjective"},
            {"word": "licensor", "class": "Noun"}
        ]
    }

Try it with Docker:

    docker run -p5000:5000 --rm registry.gitlab.com/yaunj/python-wordpair

Resources
---------

This project uses wordlists from [MOBY
Part-of-Speech](http://icon.shef.ac.uk/Moby/) and [Norsk
ordbank](http://www.edd.uio.no/prosjekt/ordbanken/).

* MOBY Part-of-Speech is in public domain (and also available on [Project
  Gutenberg](https://www.gutenberg.org/ebooks/3203))
* Norsk ordbank is licensed under the [GNU General Public
  License](http://www.gnu.org/licenses/gpl.html)
"""

setup(
    name='wordpair',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='0.0.1',

    description='Random words as a service',
    long_description=long_description,

    # The project's main homepage.
    url='https://gitlab.com/yaunj/python-wordpair/',

    # Author details
    author='Rune Hammersland',
    author_email='rune@hammersland.net',

    # Choose your license
    license='GPLv2',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Information Technology',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
        'Environment :: Web Environment',
        'Framework :: Flask',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)'

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
    ],

    # What does your project relate to?
    keywords='Flask based REST service',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),

    # Alternatively, if you want to distribute just a my_module.py, uncomment
    # this:
    #   py_modules=["my_module"],

    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=['flask'],

    # List additional groups of dependencies here (e.g. development
    # dependencies). You can install these using the following syntax,
    # for example:
    # $ pip install -e .[dev,test]
    extras_require={
        #'dev': ['check-manifest'],
        'test': ['tox'],
    },

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    package_data={
        'wordpair': [
            'resources/ordbank/fullform_bm.txt',
            'resources/mpos/mobyposi.i'
        ],
    },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files # noqa
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    #data_files=[('my_data', ['README.md'])],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': [
            'wordpair=wordpair:main',
        ],
    },
)
