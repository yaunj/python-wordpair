wordpair
========

Contains a dictionary with information about word classes. Based on this we
generate random word pairs.

Hit up /en/ for a word pair:

    {
        "phrase": "unavoidable licensor",
        "words": [
            {"word": "unavoidable", "class": "Adjective"},
            {"word": "licensor", "class": "Noun"}
        ]
    }

Try it with Docker:

    docker run -p5000:5000 --rm registry.gitlab.com/yaunj/python-wordpair

Resources
---------

This project uses wordlists from [MOBY
Part-of-Speech](http://icon.shef.ac.uk/Moby/) and [Norsk
ordbank](http://www.edd.uio.no/prosjekt/ordbanken/).

* MOBY Part-of-Speech is in public domain (and also available on [Project
  Gutenberg](https://www.gutenberg.org/ebooks/3203))
* Norsk ordbank is licensed under the [GNU General Public
  License](http://www.gnu.org/licenses/gpl.html)
