from helpers import json_req


def test_index(app):
    data = json_req(app, '/')

    assert 'links' in data
    assert len(data['links'])


def test_v1_index(app):
    data = json_req(app, '/v1/')

    assert 'links' in data
    assert len(data['links'])
