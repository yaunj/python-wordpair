from helpers import json_req, phrase_object_tests


def test_no(app):
    data = json_req(app, '/v1/no/')

    phrase_object_tests(data, ('ordbank_class', 'meta'))

    # assert two words
    assert len(data['words']) == 2
    assert len(data['phrase'].split(' ')) == 2


def test_no_multi(app):
    data = json_req(app, '/v1/no/subst')

    phrase_object_tests(data, ('ordbank_class', 'meta'))

    # assert one word
    assert len(data['words']) == 1
    assert len(data['phrase'].split(' ')) == 1


def test_no_wordclasses(app):
    data = json_req(app, '/v1/no/wordclasses/')

    assert type(data) == dict
    assert len(data.keys())


def test_no_randomness(app):
    result1 = app.get('/v1/no/').data
    result2 = app.get('/v1/no/').data

    assert result1 != result2
