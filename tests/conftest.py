import pytest

import wordpair


@pytest.fixture()
def app():
    return wordpair.app.test_client()
