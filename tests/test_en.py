from helpers import json_req, phrase_object_tests


def test_en(app):
    data = json_req(app, '/v1/en/')

    phrase_object_tests(data, ('moby_classes',))

    # assert two words
    assert len(data['words']) == 2
    assert len(data['phrase'].split(' ')) == 2


def test_en_multi(app):
    data = json_req(app, '/v1/en/N')

    phrase_object_tests(data, ('moby_classes',))

    # assert one word
    assert len(data['words']) == 1
    assert len(data['phrase'].split(' ')) == 1


def test_en_wordclasses(app):
    data = json_req(app, '/v1/en/wordclasses/')

    assert type(data) == dict
    assert len(data.keys())


def test_en_randomness(app):
    result1 = app.get('/v1/en/').data
    result2 = app.get('/v1/en/').data

    assert result1 != result2
