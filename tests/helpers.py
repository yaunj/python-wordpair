import json


def json_req(app, req):
    rv = app.get(req, headers={'Content-Type': 'application/json'})
    assert rv.status_code == 200
    return json.loads(rv.data.decode('utf-8'))


def phrase_object_tests(result, word_extra):
    assert 'phrase' in result
    assert 'words' in result

    assert result['phrase'] != ''
    assert len(result['words']) > 0

    for wordobj in result['words']:
        assert 'word' in wordobj
        for extra in word_extra:
            assert extra in wordobj

    assert result['phrase'] == ' '.join(w['word'] for w in result['words'])
